import cv2
import numpy as np
from matplotlib import pyplot as plt

 
def main():
   
    camaraWeb()
    return

def camaraWeb():
   
    cap = cv2.VideoCapture(0)
 
    while(True):
        ret,frame = cap.read()
   
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        edges = cv2.Canny(frame,100,200)
        cv2.imshow('Edge',edges)
       
       
        cv2.imshow('frame',frame)
       
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()
if __name__ == '__main__':
    main()